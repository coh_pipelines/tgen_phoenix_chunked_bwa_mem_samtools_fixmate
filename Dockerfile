ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_opt_bwa:0.7.17 AS opt_bwa
FROM ${SOURCE_DOCKER_REGISTRY}/alpine_opt_samtools:1.10 AS opt_samtools

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_base:3.11.3

COPY --from=opt_bwa /opt /opt
COPY --from=opt_samtools /opt /opt

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV LD_LIBRARY_PATH /opt/lib/

RUN mkdir -p /opt/bin/ && echo "#!/bin/bash" > /opt/bin/module && chmod a+x /opt/bin/module